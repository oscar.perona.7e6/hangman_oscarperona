package com.example.hagman

import android.content.Intent
import android.graphics.Color
import android.graphics.Color.GREEN
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.hagman.databinding.ActivityGameBinding
import com.example.hagman.databinding.ActivityResultBinding

class result_activity : AppCompatActivity() {

    lateinit var binding: ActivityResultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResultBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val winer = bundle?.getString("winer")
        val errors = bundle?.getInt("errors")


        if (winer.toBoolean() == true){
            binding.gameResult.text = " HAS GANADO!!! "
            binding.gameResult.setBackgroundColor(Color.parseColor("#C9078E09"))

            when(errors){
                in 0..2 -> binding.errorsText.text = " Has ido sobrado, has tenido $errors errores "
                in 3..5 -> binding.errorsText.text = " No esta nada mal, has tenido $errors errores "
                in 6..7 -> binding.errorsText.text = " Por los pelos, has tenido $errors errores "
            }
        }
        else{
            binding.gameResult.text = " Has perdido "
            binding.gameResult.setBackgroundColor(Color.parseColor("#C9FB0000"))
        }

        binding.returnGame.setOnClickListener {
            val intent = Intent(this, activity_game::class.java)
            startActivity(intent)
        }
        binding.returnMenu.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}