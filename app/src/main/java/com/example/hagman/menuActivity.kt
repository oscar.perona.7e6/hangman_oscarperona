package com.example.hagman

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.hagman.databinding.ActivityMainBinding

class MenuActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonPlay.setOnClickListener {
            val intent = Intent(this, activity_game::class.java)
            intent.putExtra("difucult_value", binding.dificulSpinner.selectedItem.toString())
            startActivity(intent)
        }

        binding.buttonHelp.setOnClickListener {
            val builder = AlertDialog.Builder(this@MenuActivity)
            val view = layoutInflater.inflate(R.layout.dialog_help,null)

            builder.setView(view)

            val dialog = builder.create()
            dialog.show()
        }
    }
}