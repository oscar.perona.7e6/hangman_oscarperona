package com.example.hagman

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.hagman.databinding.ActivityGameBinding

class activity_game : AppCompatActivity(), View.OnClickListener {

    lateinit var binding: ActivityGameBinding

    val fotosHangman = listOf<Int>(
        R.drawable.intent0,
        R.drawable.intent1,
        R.drawable.intent2,
        R.drawable.intent3,
        R.drawable.intent4,
        R.drawable.intent5,
        R.drawable.intent6,
        R.drawable.intent6
    )

    var printLetters = ""
    val printLettersList = mutableListOf<String>()
    var randomWord = ""
    var numberTries = 0
    var numberErrors = 0
    var winer: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityGameBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val dificult = bundle?.getString("difucult_value")


        //LEE DIFICULTAD Y SELECCIONA UNA PALABRA ALEATORIA

        when (dificult) {
            "Facil" -> {
                //PALABRAS DE 5 LETRAS
                randomWord = listOf<String>(
                    "BESOS",
                    "ALTOS",
                    "ALIAS",
                    "BOTAS",
                    "CHINA",
                    "CINES",
                    "DENSA",
                    "ECHOS",
                    "LENTA",
                    "MAZOS",
                    "LIBRO",
                    "ROJOS",
                    "TOSER",
                    "ACIDO"
                ).random()
            }
            "Media" -> {
                //PALABRAS DE 8 LETRAS
                randomWord = listOf<String>(
                    "ABANDONO",
                    "ABSOLUTO",
                    "ACEITUNA",
                    "CALABAZA",
                    "CALCULAR",
                    "DEAMBULA",
                    "DESCANSO",
                    "EDIFICAR",
                    "EJEMPLAR",
                    "FANTASIA",
                    "FARMACIA",
                    "MAGNESIO",
                    "NEGOCIAR"
                ).random()
            }
            "Dificil" -> {
                //PALABRAS DE 12 LETRAS
                randomWord = listOf<String>(
                    "ADOLESCENTES",
                    "ANTAGONISTAS",
                    "COMBATIENTES",
                    "CONFIRMADORA",
                    "ARQUEOLOGICO",
                    "CALIFORNIANA",
                    "CAVERNICOLAS",
                    "HIPNOTIZADOR",
                    "PARALELISMOS",
                    "SEPULTADORES",
                    "SUPERHEROINA",
                    "TERASEGUNDOS"
                ).random()
            }
        }

        for (i in randomWord.indices){
            printLetters+=" _ "
        }

        for (i in printLetters.indices){
            if(printLetters[i] == '_') printLettersList.add(printLetters[i].toString())
        }
        binding.letters.text = printLetters

        binding.buttonA.setOnClickListener(this)
        binding.buttonB.setOnClickListener(this)
        binding.buttonC.setOnClickListener(this)
        binding.buttonD.setOnClickListener(this)
        binding.buttonE.setOnClickListener(this)
        binding.buttonF.setOnClickListener(this)
        binding.buttonG.setOnClickListener(this)
        binding.buttonH.setOnClickListener(this)
        binding.buttonI.setOnClickListener(this)
        binding.buttonJ.setOnClickListener(this)
        binding.buttonK.setOnClickListener(this)
        binding.buttonL.setOnClickListener(this)
        binding.buttonM.setOnClickListener(this)
        binding.buttonN.setOnClickListener(this)
        binding.buttonO.setOnClickListener(this)
        binding.buttonP.setOnClickListener(this)
        binding.buttonQ.setOnClickListener(this)
        binding.buttonR.setOnClickListener(this)
        binding.buttonS.setOnClickListener(this)
        binding.buttonT.setOnClickListener(this)
        binding.buttonU.setOnClickListener(this)
        binding.buttonV.setOnClickListener(this)
        binding.buttonW.setOnClickListener(this)
        binding.buttonX.setOnClickListener(this)
        binding.buttonY.setOnClickListener(this)
        binding.buttonZ.setOnClickListener(this)
        }

    @SuppressLint("SetTextI18n")
    override fun onClick(p0: View?) {
        val button = p0 as Button
        val imputLetter = button.text
        button.isEnabled = false
        button.alpha= 0.2F
        button.setBackgroundColor(Color.parseColor("#C9FB0000"))

        printLetters = ""

        if (imputLetter !in randomWord){
            numberErrors++
            binding.imageView.setBackgroundResource(fotosHangman[numberErrors])
        }
        else{
            for(i in randomWord.indices){
                if(randomWord[i].toString()==imputLetter){
                    printLettersList[i] = imputLetter
                }
            }
        }

        for (i in printLetters.indices){
            printLetters+="${printLettersList[i]} "
        }

        if("_" !in printLettersList) winer = true
        else winer = false

        if (winer == true || numberErrors > 6) {
            val intent = Intent(this,result_activity::class.java)
            intent.putExtra("winer", winer.toString())
            intent.putExtra("errors", numberErrors)
            startActivity(intent)
        }

        for (i in printLettersList.indices){
            printLetters+="${printLettersList[i]} "
        }
        binding.letters.text = printLetters
        binding.intentosText.text = " Numero de intentos: ${++numberTries} "
    }
    }
